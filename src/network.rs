use std::error;
use std::fmt;
use std::io;
use std::net::{SocketAddrV4, UdpSocket};
use self::IPv4NetworkError::*;

pub trait NetworkError : error::Error{}


pub trait Network {
    type Addr;
    type Error: NetworkError;

    fn ping(&self, addr: Self::Addr) -> Result<(), Self::Error>;
}


// IPv4 network
pub struct IPv4Network {
    ping_send_port: u16,
    ping_recv_port: u16,
}

#[derive(Debug, Clone)]
pub enum IPv4NetworkError {
    Trivial,
    WithMessage(String),
}

impl fmt::Display for IPv4NetworkError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let msg = match self {
            Trivial => "_",
            WithMessage(msg) => msg,
        };
        write!(f, "IPv4 network error: {}", msg)
    }
}

impl error::Error for IPv4NetworkError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        // Generic error, underlying cause isn't tracked.
        None
    }
}

impl From<io::Error> for IPv4NetworkError {
    fn from(_err: io::Error) -> IPv4NetworkError {
        Trivial
    }
}

impl NetworkError for IPv4NetworkError {}

impl Network for IPv4Network {
    type Addr = SocketAddrV4;
    type Error = IPv4NetworkError;

    fn ping(&self, addr: Self::Addr) -> Result<(), Self::Error> {
        let socket = UdpSocket::bind(("127.0.0.1", self.ping_send_port))?;
        socket.connect(addr)?;
        let ping_msg = "Ping msg\n";
        socket.send(ping_msg.as_ref())?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use std::str::FromStr;
    use super::*;

    #[test]
    fn test_ping() {
        let network = IPv4Network {
            ping_send_port: 48211,
            ping_recv_port: 48212,
        };
        network.ping(SocketAddrV4::new(std::net::Ipv4Addr::from_str("127.0.0.1").expect("parse success"), 48212));
    }
}
